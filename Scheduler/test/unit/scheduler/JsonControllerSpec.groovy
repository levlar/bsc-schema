package scheduler

import grails.test.mixin.TestFor
import spock.lang.Specification

/**
 * See the API for {@link grails.test.mixin.web.ControllerUnitTestMixin} for usage instructions
 */
@TestFor(JsonSlurperController)
class JsonControllerSpec extends Specification {

    def setup() {
    	super.setUp()
    	controller = new JsonSlurperController();
    }

    def cleanup() {
    }

    void "test something"() {
    }
}
