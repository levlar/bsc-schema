package scheduler

import grails.test.mixin.TestMixin
import grails.test.mixin.support.GrailsUnitTestMixin
import spock.lang.Specification

/**
 * See the API for {@link grails.test.mixin.support.GrailsUnitTestMixin} for usage instructions
 */

// 	SchemaEntry(int week, int id, int day, int start, int duration, String title, String location, String color, String time, String teacher){

@TestMixin(GrailsUnitTestMixin)
@TestFor(SchemaEntry)
class SchemaEntrySpec extends Specification {

    def setup() {
    }

    def cleanup() {
    }
	
	void "test for basic object is correct"() {
		given:
			SchemaEntry testEntry = correctSchemaEntry
			
		expect:
			testEntry.validate() == true
	}

    void "test for string attributes = null"() {
		given:
			SchemaEntry testEntry = correctSchemaEntry

		when:
			testEntry.title = null
			testEntry.color = null
			testEntry.location = null
			testEntry.time = null
			testEntry.teacher = null

		then:
			testEntry.validate() == false
    }

    void "test for int attributes < 0"() {
    	given:
    		SchemaEntry testEntry = correctSchemaEntry

    	when:
    		testEntry.week = -1
    		testEntry.day = -1
    		testEntry.start = -1
    		testEntry.duration = -1

    	then:
    		testEntry.validate() == false
    }

    void "test for .id < -1"() {
    	given:
    		SchemaEntry testEntry = correctSchemaEntry

    	when:
    		testEntry.id = -2

    	then:
    		testEntry.validate() == false
    }

    SchemaEntry getCorrectSchemaEntry(){
    	new SchemaEntry(17, 1, 1, 0, 4, "DM557", "u55", "red", "8:00-12-00", "Jacob Aae Mikkelsen")
    }
}
