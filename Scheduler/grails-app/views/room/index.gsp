
<%@ page import="scheduler.Room" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'room.label', default: 'Room')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#list-room" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="list-room" class="content scaffold-list" role="main">
			<h1><g:message code="default.list.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
				<div class="message" role="status">${flash.message}</div>
			</g:if>
			<table>
			<thead>
					<tr>
					
						<g:sortableColumn property="roomName" title="${message(code: 'room.roomName.label', default: 'Room Name')}" />
					
						<g:sortableColumn property="maxAllowed" title="${message(code: 'room.maxAllowed.label', default: 'Max Allowed')}" />
					
						<g:sortableColumn property="projector" title="${message(code: 'room.projector.label', default: 'Projector')}" />
					
						<g:sortableColumn property="whiteboard" title="${message(code: 'room.whiteboard.label', default: 'Whiteboard')}" />
					
						<g:sortableColumn property="tvScreens" title="${message(code: 'room.tvScreens.label', default: 'Tv Screens')}" />
					
						<g:sortableColumn property="speakerSystem" title="${message(code: 'room.speakerSystem.label', default: 'Speaker System')}" />
					
					</tr>
				</thead>
				<tbody>
				<g:each in="${roomInstanceList}" status="i" var="roomInstance">
					<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
					
						<td><g:link action="show" id="${roomInstance.id}">${fieldValue(bean: roomInstance, field: "roomName")}</g:link></td>
					
						<td>${fieldValue(bean: roomInstance, field: "maxAllowed")}</td>
					
						<td><g:formatBoolean boolean="${roomInstance.projector}" /></td>
					
						<td><g:formatBoolean boolean="${roomInstance.whiteboard}" /></td>
					
						<td><g:formatBoolean boolean="${roomInstance.tvScreens}" /></td>
					
						<td><g:formatBoolean boolean="${roomInstance.speakerSystem}" /></td>
					
					</tr>
				</g:each>
				</tbody>
			</table>
			<div class="pagination">
				<g:paginate total="${roomInstanceCount ?: 0}" />
			</div>
		</div>
	</body>
</html>
