<%@ page import="scheduler.Room" %>



<div class="fieldcontain ${hasErrors(bean: roomInstance, field: 'roomName', 'error')} required">
	<label for="roomName">
		<g:message code="room.roomName.label" default="Room Name" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="roomName" required="" value="${roomInstance?.roomName}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: roomInstance, field: 'maxAllowed', 'error')} required">
	<label for="maxAllowed">
		<g:message code="room.maxAllowed.label" default="Max Allowed" />
		<span class="required-indicator">*</span>
	</label>
	<g:field name="maxAllowed" type="number" min="0" value="${roomInstance.maxAllowed}" required=""/>

</div>

<div class="fieldcontain ${hasErrors(bean: roomInstance, field: 'projector', 'error')} ">
	<label for="projector">
		<g:message code="room.projector.label" default="Projector" />
		
	</label>
	<g:checkBox name="projector" value="${roomInstance?.projector}" />

</div>

<div class="fieldcontain ${hasErrors(bean: roomInstance, field: 'whiteboard', 'error')} ">
	<label for="whiteboard">
		<g:message code="room.whiteboard.label" default="Whiteboard" />
		
	</label>
	<g:checkBox name="whiteboard" value="${roomInstance?.whiteboard}" />

</div>

<div class="fieldcontain ${hasErrors(bean: roomInstance, field: 'tvScreens', 'error')} ">
	<label for="tvScreens">
		<g:message code="room.tvScreens.label" default="Tv Screens" />
		
	</label>
	<g:checkBox name="tvScreens" value="${roomInstance?.tvScreens}" />

</div>

<div class="fieldcontain ${hasErrors(bean: roomInstance, field: 'speakerSystem', 'error')} ">
	<label for="speakerSystem">
		<g:message code="room.speakerSystem.label" default="Speaker System" />
		
	</label>
	<g:checkBox name="speakerSystem" value="${roomInstance?.speakerSystem}" />

</div>

