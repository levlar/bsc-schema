
<%@ page import="scheduler.Room" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'room.label', default: 'Room')}" />
		<title><g:message code="default.show.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#show-room" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="list" action="index"><g:message code="default.list.label" args="[entityName]" /></g:link></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="show-room" class="content scaffold-show" role="main">
			<h1><g:message code="default.show.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<ol class="property-list room">
			
				<g:if test="${roomInstance?.roomName}">
				<li class="fieldcontain">
					<span id="roomName-label" class="property-label"><g:message code="room.roomName.label" default="Room Name" /></span>
					
						<span class="property-value" aria-labelledby="roomName-label"><g:fieldValue bean="${roomInstance}" field="roomName"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${roomInstance?.maxAllowed}">
				<li class="fieldcontain">
					<span id="maxAllowed-label" class="property-label"><g:message code="room.maxAllowed.label" default="Max Allowed" /></span>
					
						<span class="property-value" aria-labelledby="maxAllowed-label"><g:fieldValue bean="${roomInstance}" field="maxAllowed"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${roomInstance?.projector}">
				<li class="fieldcontain">
					<span id="projector-label" class="property-label"><g:message code="room.projector.label" default="Projector" /></span>
					
						<span class="property-value" aria-labelledby="projector-label"><g:formatBoolean boolean="${roomInstance?.projector}" /></span>
					
				</li>
				</g:if>
			
				<g:if test="${roomInstance?.whiteboard}">
				<li class="fieldcontain">
					<span id="whiteboard-label" class="property-label"><g:message code="room.whiteboard.label" default="Whiteboard" /></span>
					
						<span class="property-value" aria-labelledby="whiteboard-label"><g:formatBoolean boolean="${roomInstance?.whiteboard}" /></span>
					
				</li>
				</g:if>
			
				<g:if test="${roomInstance?.tvScreens}">
				<li class="fieldcontain">
					<span id="tvScreens-label" class="property-label"><g:message code="room.tvScreens.label" default="Tv Screens" /></span>
					
						<span class="property-value" aria-labelledby="tvScreens-label"><g:formatBoolean boolean="${roomInstance?.tvScreens}" /></span>
					
				</li>
				</g:if>
			
				<g:if test="${roomInstance?.speakerSystem}">
				<li class="fieldcontain">
					<span id="speakerSystem-label" class="property-label"><g:message code="room.speakerSystem.label" default="Speaker System" /></span>
					
						<span class="property-value" aria-labelledby="speakerSystem-label"><g:formatBoolean boolean="${roomInstance?.speakerSystem}" /></span>
					
				</li>
				</g:if>
			
			</ol>
			<g:form url="[resource:roomInstance, action:'delete']" method="DELETE">
				<fieldset class="buttons">
					<g:link class="edit" action="edit" resource="${roomInstance}"><g:message code="default.button.edit.label" default="Edit" /></g:link>
					<g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
				</fieldset>
			</g:form>
		</div>
	</body>
</html>
