<%@ page import="scheduler.Student" %>



<div class="fieldcontain ${hasErrors(bean: studentInstance, field: 'studentName', 'error')} required">
	<label for="studentName">
		<g:message code="student.studentName.label" default="Student Name" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="studentName" required="" value="${studentInstance?.studentName}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: studentInstance, field: 'studentSubjects', 'error')} required">
	<label for="studentSubjects">
		<g:message code="student.studentSubjects.label" default="Student Subjects" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="studentSubjects" required="" value="${studentInstance?.studentSubjects}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: studentInstance, field: 'busy', 'error')} ">
	<label for="busy">
		<g:message code="student.busy.label" default="Busy" />
		
	</label>
	<g:textField name="busy" value="${studentInstance?.busy}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: studentInstance, field: 'type', 'error')} required">
	<label for="type">
		<g:message code="student.type.label" default="Type" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="type" required="" value="${studentInstance?.type}"/>

</div>

