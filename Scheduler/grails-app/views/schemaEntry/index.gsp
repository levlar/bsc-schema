
<%@ page import="scheduler.SchemaEntry" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'schemaEntry.label', default: 'SchemaEntry')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#list-schemaEntry" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="list-schemaEntry" class="content scaffold-list" role="main">
			<h1><g:message code="default.list.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
				<div class="message" role="status">${flash.message}</div>
			</g:if>
			<table>
			<thead>
					<tr>
					
						<g:sortableColumn property="week" title="${message(code: 'schemaEntry.week.label', default: 'Week')}" />
					
						<g:sortableColumn property="entryId" title="${message(code: 'schemaEntry.entryId.label', default: 'Entry Id')}" />
					
						<g:sortableColumn property="day" title="${message(code: 'schemaEntry.day.label', default: 'Day')}" />
					
						<g:sortableColumn property="start" title="${message(code: 'schemaEntry.start.label', default: 'Start')}" />
					
						<g:sortableColumn property="duration" title="${message(code: 'schemaEntry.duration.label', default: 'Duration')}" />
					
						<g:sortableColumn property="year" title="${message(code: 'schemaEntry.year.label', default: 'Year')}" />
					
					</tr>
				</thead>
				<tbody>
				<g:each in="${schemaEntryInstanceList}" status="i" var="schemaEntryInstance">
					<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
					
						<td><g:link action="show" id="${schemaEntryInstance.id}">${fieldValue(bean: schemaEntryInstance, field: "week")}</g:link></td>
					
						<td>${fieldValue(bean: schemaEntryInstance, field: "entryId")}</td>
					
						<td>${fieldValue(bean: schemaEntryInstance, field: "day")}</td>
					
						<td>${fieldValue(bean: schemaEntryInstance, field: "start")}</td>
					
						<td>${fieldValue(bean: schemaEntryInstance, field: "duration")}</td>
					
						<td>${fieldValue(bean: schemaEntryInstance, field: "year")}</td>
					
					</tr>
				</g:each>
				</tbody>
			</table>
			<div class="pagination">
				<g:paginate total="${schemaEntryInstanceCount ?: 0}" />
			</div>
		</div>
	</body>
</html>
