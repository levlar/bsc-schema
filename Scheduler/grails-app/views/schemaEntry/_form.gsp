<%@ page import="scheduler.SchemaEntry" %>



<div class="fieldcontain ${hasErrors(bean: schemaEntryInstance, field: 'week', 'error')} required">
	<label for="week">
		<g:message code="schemaEntry.week.label" default="Week" />
		<span class="required-indicator">*</span>
	</label>
	<g:field name="week" type="number" min="-1" value="${schemaEntryInstance.week}" required=""/>

</div>

<div class="fieldcontain ${hasErrors(bean: schemaEntryInstance, field: 'entryId', 'error')} required">
	<label for="entryId">
		<g:message code="schemaEntry.entryId.label" default="Entry Id" />
		<span class="required-indicator">*</span>
	</label>
	<g:field name="entryId" type="number" min="-1" value="${schemaEntryInstance.entryId}" required=""/>

</div>

<div class="fieldcontain ${hasErrors(bean: schemaEntryInstance, field: 'day', 'error')} required">
	<label for="day">
		<g:message code="schemaEntry.day.label" default="Day" />
		<span class="required-indicator">*</span>
	</label>
	<g:field name="day" type="number" min="-1" value="${schemaEntryInstance.day}" required=""/>

</div>

<div class="fieldcontain ${hasErrors(bean: schemaEntryInstance, field: 'start', 'error')} required">
	<label for="start">
		<g:message code="schemaEntry.start.label" default="Start" />
		<span class="required-indicator">*</span>
	</label>
	<g:field name="start" type="number" min="-1" value="${schemaEntryInstance.start}" required=""/>

</div>

<div class="fieldcontain ${hasErrors(bean: schemaEntryInstance, field: 'duration', 'error')} required">
	<label for="duration">
		<g:message code="schemaEntry.duration.label" default="Duration" />
		<span class="required-indicator">*</span>
	</label>
	<g:field name="duration" type="number" min="-1" value="${schemaEntryInstance.duration}" required=""/>

</div>

<div class="fieldcontain ${hasErrors(bean: schemaEntryInstance, field: 'year', 'error')} required">
	<label for="year">
		<g:message code="schemaEntry.year.label" default="Year" />
		<span class="required-indicator">*</span>
	</label>
	<g:field name="year" type="number" min="-1" value="${schemaEntryInstance.year}" required=""/>

</div>

<div class="fieldcontain ${hasErrors(bean: schemaEntryInstance, field: 'title', 'error')} required">
	<label for="title">
		<g:message code="schemaEntry.title.label" default="Title" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="title" required="" value="${schemaEntryInstance?.title}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: schemaEntryInstance, field: 'location', 'error')} required">
	<label for="location">
		<g:message code="schemaEntry.location.label" default="Location" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="location" required="" value="${schemaEntryInstance?.location}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: schemaEntryInstance, field: 'color', 'error')} required">
	<label for="color">
		<g:message code="schemaEntry.color.label" default="Color" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="color" required="" value="${schemaEntryInstance?.color}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: schemaEntryInstance, field: 'time', 'error')} required">
	<label for="time">
		<g:message code="schemaEntry.time.label" default="Time" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="time" required="" value="${schemaEntryInstance?.time}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: schemaEntryInstance, field: 'overlap', 'error')} required">
	<label for="overlap">
		<g:message code="schemaEntry.overlap.label" default="Overlap" />
		<span class="required-indicator">*</span>
	</label>
	<g:field name="overlap" type="number" value="${schemaEntryInstance.overlap}" required=""/>

</div>

