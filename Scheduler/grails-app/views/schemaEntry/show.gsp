
<%@ page import="scheduler.SchemaEntry" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'schemaEntry.label', default: 'SchemaEntry')}" />
		<title><g:message code="default.show.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#show-schemaEntry" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="list" action="index"><g:message code="default.list.label" args="[entityName]" /></g:link></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="show-schemaEntry" class="content scaffold-show" role="main">
			<h1><g:message code="default.show.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<ol class="property-list schemaEntry">
			
				<g:if test="${schemaEntryInstance?.week}">
				<li class="fieldcontain">
					<span id="week-label" class="property-label"><g:message code="schemaEntry.week.label" default="Week" /></span>
					
						<span class="property-value" aria-labelledby="week-label"><g:fieldValue bean="${schemaEntryInstance}" field="week"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${schemaEntryInstance?.entryId}">
				<li class="fieldcontain">
					<span id="entryId-label" class="property-label"><g:message code="schemaEntry.entryId.label" default="Entry Id" /></span>
					
						<span class="property-value" aria-labelledby="entryId-label"><g:fieldValue bean="${schemaEntryInstance}" field="entryId"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${schemaEntryInstance?.day}">
				<li class="fieldcontain">
					<span id="day-label" class="property-label"><g:message code="schemaEntry.day.label" default="Day" /></span>
					
						<span class="property-value" aria-labelledby="day-label"><g:fieldValue bean="${schemaEntryInstance}" field="day"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${schemaEntryInstance?.start}">
				<li class="fieldcontain">
					<span id="start-label" class="property-label"><g:message code="schemaEntry.start.label" default="Start" /></span>
					
						<span class="property-value" aria-labelledby="start-label"><g:fieldValue bean="${schemaEntryInstance}" field="start"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${schemaEntryInstance?.duration}">
				<li class="fieldcontain">
					<span id="duration-label" class="property-label"><g:message code="schemaEntry.duration.label" default="Duration" /></span>
					
						<span class="property-value" aria-labelledby="duration-label"><g:fieldValue bean="${schemaEntryInstance}" field="duration"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${schemaEntryInstance?.year}">
				<li class="fieldcontain">
					<span id="year-label" class="property-label"><g:message code="schemaEntry.year.label" default="Year" /></span>
					
						<span class="property-value" aria-labelledby="year-label"><g:fieldValue bean="${schemaEntryInstance}" field="year"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${schemaEntryInstance?.title}">
				<li class="fieldcontain">
					<span id="title-label" class="property-label"><g:message code="schemaEntry.title.label" default="Title" /></span>
					
						<span class="property-value" aria-labelledby="title-label"><g:fieldValue bean="${schemaEntryInstance}" field="title"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${schemaEntryInstance?.location}">
				<li class="fieldcontain">
					<span id="location-label" class="property-label"><g:message code="schemaEntry.location.label" default="Location" /></span>
					
						<span class="property-value" aria-labelledby="location-label"><g:fieldValue bean="${schemaEntryInstance}" field="location"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${schemaEntryInstance?.color}">
				<li class="fieldcontain">
					<span id="color-label" class="property-label"><g:message code="schemaEntry.color.label" default="Color" /></span>
					
						<span class="property-value" aria-labelledby="color-label"><g:fieldValue bean="${schemaEntryInstance}" field="color"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${schemaEntryInstance?.time}">
				<li class="fieldcontain">
					<span id="time-label" class="property-label"><g:message code="schemaEntry.time.label" default="Time" /></span>
					
						<span class="property-value" aria-labelledby="time-label"><g:fieldValue bean="${schemaEntryInstance}" field="time"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${schemaEntryInstance?.overlap}">
				<li class="fieldcontain">
					<span id="overlap-label" class="property-label"><g:message code="schemaEntry.overlap.label" default="Overlap" /></span>
					
						<span class="property-value" aria-labelledby="overlap-label"><g:fieldValue bean="${schemaEntryInstance}" field="overlap"/></span>
					
				</li>
				</g:if>
			
			</ol>
			<g:form url="[resource:schemaEntryInstance, action:'delete']" method="DELETE">
				<fieldset class="buttons">
					<g:link class="edit" action="edit" resource="${schemaEntryInstance}"><g:message code="default.button.edit.label" default="Edit" /></g:link>
					<g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
				</fieldset>
			</g:form>
		</div>
	</body>
</html>
