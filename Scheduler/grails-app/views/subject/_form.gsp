<%@ page import="scheduler.Subject" %>



<div class="fieldcontain ${hasErrors(bean: subjectInstance, field: 'subjectName', 'error')} required">
	<label for="subjectName">
		<g:message code="subject.subjectName.label" default="Subject Name" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="subjectName" required="" value="${subjectInstance?.subjectName}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: subjectInstance, field: 'subjectTeacher', 'error')} required">
	<label for="subjectTeacher">
		<g:message code="subject.subjectTeacher.label" default="Subject Teacher" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="subjectTeacher" required="" value="${subjectInstance?.subjectTeacher}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: subjectInstance, field: 'studentsEnrolled', 'error')} required">
	<label for="studentsEnrolled">
		<g:message code="subject.studentsEnrolled.label" default="Students Enrolled" />
		<span class="required-indicator">*</span>
	</label>
	<g:textArea name="studentsEnrolled" cols="40" rows="5" maxlength="10000" required="" value="${subjectInstance?.studentsEnrolled}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: subjectInstance, field: 'subjectWeekStart', 'error')} required">
	<label for="subjectWeekStart">
		<g:message code="subject.subjectWeekStart.label" default="Subject Week Start" />
		<span class="required-indicator">*</span>
	</label>
	<g:field name="subjectWeekStart" type="number" min="0" value="${subjectInstance.subjectWeekStart}" required=""/>

</div>

<div class="fieldcontain ${hasErrors(bean: subjectInstance, field: 'subjectWeekEnd', 'error')} required">
	<label for="subjectWeekEnd">
		<g:message code="subject.subjectWeekEnd.label" default="Subject Week End" />
		<span class="required-indicator">*</span>
	</label>
	<g:field name="subjectWeekEnd" type="number" min="0" value="${subjectInstance.subjectWeekEnd}" required=""/>

</div>

<div class="fieldcontain ${hasErrors(bean: subjectInstance, field: 'subjectYear', 'error')} required">
	<label for="subjectYear">
		<g:message code="subject.subjectYear.label" default="Subject Year" />
		<span class="required-indicator">*</span>
	</label>
	<g:field name="subjectYear" type="number" min="0" value="${subjectInstance.subjectYear}" required=""/>

</div>

