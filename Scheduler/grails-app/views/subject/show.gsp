
<%@ page import="scheduler.Subject" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'subject.label', default: 'Subject')}" />
		<title><g:message code="default.show.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#show-subject" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="list" action="index"><g:message code="default.list.label" args="[entityName]" /></g:link></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="show-subject" class="content scaffold-show" role="main">
			<h1><g:message code="default.show.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<ol class="property-list subject">
			
				<g:if test="${subjectInstance?.subjectName}">
				<li class="fieldcontain">
					<span id="subjectName-label" class="property-label"><g:message code="subject.subjectName.label" default="Subject Name" /></span>
					
						<span class="property-value" aria-labelledby="subjectName-label"><g:fieldValue bean="${subjectInstance}" field="subjectName"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${subjectInstance?.subjectTeacher}">
				<li class="fieldcontain">
					<span id="subjectTeacher-label" class="property-label"><g:message code="subject.subjectTeacher.label" default="Subject Teacher" /></span>
					
						<span class="property-value" aria-labelledby="subjectTeacher-label"><g:fieldValue bean="${subjectInstance}" field="subjectTeacher"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${subjectInstance?.studentsEnrolled}">
				<li class="fieldcontain">
					<span id="studentsEnrolled-label" class="property-label"><g:message code="subject.studentsEnrolled.label" default="Students Enrolled" /></span>
					
						<span class="property-value" aria-labelledby="studentsEnrolled-label"><g:fieldValue bean="${subjectInstance}" field="studentsEnrolled"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${subjectInstance?.subjectWeekStart}">
				<li class="fieldcontain">
					<span id="subjectWeekStart-label" class="property-label"><g:message code="subject.subjectWeekStart.label" default="Subject Week Start" /></span>
					
						<span class="property-value" aria-labelledby="subjectWeekStart-label"><g:fieldValue bean="${subjectInstance}" field="subjectWeekStart"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${subjectInstance?.subjectWeekEnd}">
				<li class="fieldcontain">
					<span id="subjectWeekEnd-label" class="property-label"><g:message code="subject.subjectWeekEnd.label" default="Subject Week End" /></span>
					
						<span class="property-value" aria-labelledby="subjectWeekEnd-label"><g:fieldValue bean="${subjectInstance}" field="subjectWeekEnd"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${subjectInstance?.subjectYear}">
				<li class="fieldcontain">
					<span id="subjectYear-label" class="property-label"><g:message code="subject.subjectYear.label" default="Subject Year" /></span>
					
						<span class="property-value" aria-labelledby="subjectYear-label"><g:fieldValue bean="${subjectInstance}" field="subjectYear"/></span>
					
				</li>
				</g:if>
			
			</ol>
			<g:form url="[resource:subjectInstance, action:'delete']" method="DELETE">
				<fieldset class="buttons">
					<g:link class="edit" action="edit" resource="${subjectInstance}"><g:message code="default.button.edit.label" default="Edit" /></g:link>
					<g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
				</fieldset>
			</g:form>
		</div>
	</body>
</html>
