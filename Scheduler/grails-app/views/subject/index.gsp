
<%@ page import="scheduler.Subject" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'subject.label', default: 'Subject')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#list-subject" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="list-subject" class="content scaffold-list" role="main">
			<h1><g:message code="default.list.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
				<div class="message" role="status">${flash.message}</div>
			</g:if>
			<table>
			<thead>
					<tr>
					
						<g:sortableColumn property="subjectName" title="${message(code: 'subject.subjectName.label', default: 'Subject Name')}" />
					
						<g:sortableColumn property="subjectTeacher" title="${message(code: 'subject.subjectTeacher.label', default: 'Subject Teacher')}" />
					
						<g:sortableColumn property="studentsEnrolled" title="${message(code: 'subject.studentsEnrolled.label', default: 'Students Enrolled')}" />
					
						<g:sortableColumn property="subjectWeekStart" title="${message(code: 'subject.subjectWeekStart.label', default: 'Subject Week Start')}" />
					
						<g:sortableColumn property="subjectWeekEnd" title="${message(code: 'subject.subjectWeekEnd.label', default: 'Subject Week End')}" />
					
						<g:sortableColumn property="subjectYear" title="${message(code: 'subject.subjectYear.label', default: 'Subject Year')}" />
					
					</tr>
				</thead>
				<tbody>
				<g:each in="${subjectInstanceList}" status="i" var="subjectInstance">
					<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
					
						<td><g:link action="show" id="${subjectInstance.id}">${fieldValue(bean: subjectInstance, field: "subjectName")}</g:link></td>
					
						<td>${fieldValue(bean: subjectInstance, field: "subjectTeacher")}</td>
					
						<td>${fieldValue(bean: subjectInstance, field: "studentsEnrolled")}</td>
					
						<td>${fieldValue(bean: subjectInstance, field: "subjectWeekStart")}</td>
					
						<td>${fieldValue(bean: subjectInstance, field: "subjectWeekEnd")}</td>
					
						<td>${fieldValue(bean: subjectInstance, field: "subjectYear")}</td>
					
					</tr>
				</g:each>
				</tbody>
			</table>
			<div class="pagination">
				<g:paginate total="${subjectInstanceCount ?: 0}" />
			</div>
		</div>
	</body>
</html>
