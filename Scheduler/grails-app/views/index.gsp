<%@ page contentType="text/html;charset=UTF-8" %>
<html>
	<head>
		<meta name="layout" content="main"/>
		<title>Welcome to Scheduler</title>
	</head>
	<body>
		<nav class="navbar navbar-default navbar-fixed-top">
			<div class="row" id="plannerHeaderRow">
				<div class="col-md-1"><div id="schemaHome" role="banner"><a href="/Scheduler/"><asset:image src="home_small.png" alt="Scheduler"/></a></div></div>
				<div class="col-md-2"></div>
				<div class="col-md-2"></div>
				<div class="col-md-2"></div>
				<div class="col-md-2"></div>
				<div class="col-md-2"></div>
			 	<div class="col-md-1">
					<g:link controller="${params.controller}" action="${params.action}" params="[lang:'en']" class="menuButton"><asset:image class="navFlag" src="gb.png"/></g:link>
	 				<g:link controller="${params.controller}" action="${params.action}" params="[lang:'da']" class="menuButton"><asset:image class="navFlag" src="dk.png"/></g:link>
	 			</div>
			</div>		  
		</nav>
		<div class="container pushTop">
			<div class="row">
				<div class="col-md-1"></div>
				<div class="col-md-4">
					<g:form controller="JsonSlurper" action="uploadSchemaEntries" enctype="multipart/form-data">
						<div id="indexUploadHeader"><g:message code="intro.schedule"/></div>
						<div><input type='file' name="cfile" class="btn btn-info smallBorder"/></div>
						<br/>
						<input type="submit" value="<g:message code="intro.schedule.upload"/>" class="btn btn-success smallBorder"/>
					</g:form>
					<br/>
					<button class="btn btn-danger" id="SchemaEntry" onClick=dropTableNow(this.id)><g:message code="intro.schedule.clear"/></button>
					<br/><br/><br/>
					<g:form controller="JsonSlurper" action="uploadStudents" enctype="multipart/form-data">
						<p id="indexUploadHeader"><g:message code="intro.student"/></p>
						<p><input type='file' name="cfile" class="btn btn-info smallBorder"/></p>
						<br/>
						<input type="submit" value="<g:message code="intro.student.upload"/>" class="btn btn-success smallBorder"/>
					</g:form>
					<br/>
					<button class="btn btn-danger" id="Student" onClick=dropTableNow(this.id)><g:message code="intro.student.clear"/></button>
				</div>
				<div class="col-md-4">
					<g:form controller="JsonSlurper" action="uploadRooms" enctype="multipart/form-data">
						<p id="indexUploadHeader"><g:message code="intro.room"/></p>
						<p><input type='file' name="cfile" class="btn btn-info smallBorder"/></p>
						<br/>
						<input type="submit" value="<g:message code="intro.room.upload"/>" class="btn btn-success smallBorder"/>
					</g:form>
					<br/>
					<button class="btn btn-danger" id="Room" onClick=dropTableNow(this.id)><g:message code="intro.room.clear"/></button>
					<br/><br/><br/>
					<g:form controller="JsonSlurper" action="uploadSubjects" enctype="multipart/form-data">
						<p id="indexUploadHeader"><g:message code="intro.subject"/></p>
						<p><input type='file' name="cfile" class="btn btn-info smallBorder"/></p>
						<br/>
						<input type="submit" value="<g:message code="intro.subject.upload"/>" class="btn btn-success smallBorder"/>
					</g:form>
					<br/>
					<button class="btn btn-danger" id="Subject" onClick=dropTableNow(this.id)><g:message code="intro.subject.clear"/></button>
				</div>
				<div class="col-md-2">
					<br/><br/>
					<br/><br/>
					<br/><br/>
					<g:link controller="WeekPlanner" action="index"> 
						<input type="button" value="<g:message code="intro.show.planner"/>" class="btn btn-primary"/> 
					</g:link>
					<br/><br/>
					<br/><br/>
					<g:link controller="WeekPlanner" action="export"> 
						<input type="button" value="<g:message code="intro.export"/>" class="btn btn-warning"/> 
					</g:link>
			<!-- 	<br/><br/>
					<g:link controller="WeekTeacher" action="index"> 
					   <input type="button" value="<g:message code="intro.show.teacher"/>" class="btn btn-warning"/> 
					</g:link>-->
				</div>
			</div>
			<br><br><br>
			<div class="row">
				<div class="col-md-3"></div>
				<div class="col-md-5" id="indexStatus">
					<g:if test="${flash.message}">
						<div class="message" role="status" id="indexFlashDiv">${flash.message}</div>
					</g:if>
				</div>
			</div>
		</div>
	</body>
</html>
