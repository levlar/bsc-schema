<%@ page contentType="text/html;charset=UTF-8" %>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
		<meta name="layout" content="main"/>
		<title><g:message code="Scheduler Week"/></title>
	</head>
	<body>
		<nav class="navbar navbar-default navbar-fixed-top">
			<div class="row" id="plannerHeaderRow">
				<div class="col-md-1"><div id="schemaHome" role="banner"><a href="/Scheduler/"><asset:image src="home_small.png" alt="Scheduler"/></a></div></div>
				<div class="col-md-1"><h2 class="navItem" ><g:message code="week.teacher.number" args="${nuWeek}"/></h2></div>
				<div class="col-md-1"><h2 class="navItem">${nuYear}</h2></div>
				<div class="col-md-1"><button class="btn btn-navi navItem fontWeek" onclick="changeWeekPlanner(this.value)" value="-1"><span class="glyphicon glyphicon-arrow-left"/> <g:message code="week.teacher.change.earlier"/></button></div>
				<div class="col-md-2 weekHeader"><h3 class="navItem"><g:message code="week.teacher.change"/></h3></div>
				<div class="col-md-1"><button class="btn btn-navi navItem fontWeek" onclick="changeWeekPlanner(this.value)" value="1"><g:message code="week.teacher.change.later"/> <span class="glyphicon glyphicon-arrow-right"/></button></div>
				<div class="col-md-2">
					<div class="input-group navItem">
					    <input type="number" class="form-control" id="enterWeekInput" placeholder="<g:message code="week.planner.search"/>">
					    <span class="input-group-btn navButton">
							<button class="btn btn-secondary" onclick="enterWeekPlanner()" type="button"><span class="glyphicon glyphicon-search"/></button>
					    </span>
				    </div>
				</div>
				<div class="col-md-1">
					<g:link controller="WeekPlanner" action="index"> 
					   <input type="button" value="<g:message code="week.planner.reset.view"/>" class="btn btn-danger navButton"/> 
					</g:link>
				</div>	
				<div class="col-md-1">
					<button type="button" class="btn btn-success navButton" data-toggle="modal" data-target="#myModal"><g:message code="week.planner.display"/></button>
				</div>
			 	<div class="col-md-1">
			 		<div class="pull-right pushRight">
						<g:link controller="${params.controller}" action="${params.action}" params="[lang:'en', viewString:viewString]" class="menuButton"><asset:image class="navFlag" src="gb.png"/></g:link>
	 					<g:link controller="${params.controller}" action="${params.action}" params="[lang:'da', viewString:viewString]" class="menuButton"><asset:image class="navFlag" src="dk.png"/></g:link>
			 		</div>
	 			</div>
			</div>		  
		</nav>
		<br/><br/>
		<div class="row">
			<div class="col-md-10">
			 	<table class="teacher"> 
				 	<tr>
				 		<td></td>
				 		<td id="tableHeader"><g:message code="Monday"/></td>
				 		<td id="tableHeader"><g:message code="Tuesday"/></td>
				 		<td id="tableHeader"><g:message code="Wednesday"/></td>
				 		<td id="tableHeader"><g:message code="Thursday"/></td>
				 		<td id="tableHeader"><g:message code="Friday"/></td>
				 	</tr>
				 	<g:each in="${0..10}" status="i" var="hour">
				 		<tr>
					 		<td id="tableTimeColumn">
					 			${8 + i + ":00 - " + (9 + i) + ":00"}
					 		</td>  
					 		<g:each in="${0..4}" status="j" var="day">
					 			<td id="${j + '_' + i}" ondrop="dropPlanner(event)" ondragover="allowDrop(event)" ondragleave="leaveDrop(event)" style="width:18%">
					 				<div class="row">
					 					<div class="col-md-10">
						 					<g:each in="${queryResult}" var="schemaEntry" >
								 				<g:if test="${schemaEntry.day == day && schemaEntry.start == hour}">
								 					<div onclick="clickSubjectPlanner(this.id)" ondragstart="drag(event)" id="${j + '_' + i + '_' + schemaEntry.id}" class="btn ${schemaEntry.color} btnSchema" >
									 					${schemaEntry.title}					 					
														<br/>
														${schemaEntry.location}
													</div>	
								 				</g:if>
						 					</g:each>
					 					</div>
					 				<!--	<div class="col-md-2" data-toggle="modal" data-target="#squareModal">-->
					 					<div class="col-md-2">
					 						<div class="row">
					 							<div class="col-md-12 squareColor" id="${'teacherSquare' + j + '_' + i}">
					 								<br/>
				 								</div>
					 						</div>
					 						<div class="row">
					 							<div class="col-md-12 squareColor" id="${'locationSquare' + j + '_' + i}">
					 								<br/>
				 								</div>
					 						</div>
					 						<div class="row">
					 							<div class="col-md-12 squareColor" id="${'overlapSquare' + j + '_' + i}">
					 								<br/>
				 								</div>
					 						</div>
					 					</div>
					 				</div>	 			
						 		</td>
					 		</g:each>
				 		</tr>
				 	</g:each>
			 	</table>
			 </div>
			<div class="col-md-2">
				<g:if test="${flash.message}">
					<div class="message" role="status" id="indexFlashDiv">${flash.message}</div>
				</g:if>
				<div class="row">
					<div class="col-md-12" id="legendId" style="visibility:hidden">
					<h2><g:message code="week.planner.legend"/></h2><br/><br/>
						<div class="row">
							<div class="col-md-3 legendColor" id="legendDarkRed" style="visibility:hidden">_</div><div class="col-md-9" id="legendDarkRedText" style="visibility:hidden"><g:message code="week.planner.legend.teacher"/></div>
						</div>
						<br/>
						<div class="row">
							<div class="col-md-3 legendColor" id="legendOrange" style="visibility:hidden">_</div><div class="col-md-9" id="legendOrangeText" style="visibility:hidden"><g:message code="week.planner.legend.room"/></div>
						</div>
						<br/>
						<div class="row">
							<div class="col-md-3 legendColor" id="legendYellow" style="visibility:hidden">_</div><div class="col-md-9" id="legendYellowText" style="visibility:hidden"><g:message code="week.planner.legend.overlap"/></div>
						</div>
					</div>
				</div>
				<br/><br/>
				<div class="row">
					<div class="legendText" id="infoContainer" style="visibility:hidden">
						<h3>Info center:</h3><br/><br/>
					</div>
				</div>
			</div>
		</div>
		<div id="myModal" class="modal fade" role="dialog">
		  <div class="modal-dialog">
		    <!-- Modal content-->
		    <div class="modal-content">
		      <div class="modal-header">
		        <button type="button" class="close" data-dismiss="modal">&times;</button>
		        <h4 class="modal-title"><g:message code="week.planner.display.header"/></h4>
		      </div>
		      <div class="modal-body">     
	    		<g:form class="displaySpecificInner" name="roomSearch" controller="weekPlanner" action="roomView" params="[room: roomString]">
					<g:textField name="roomString" placeholder=" room"/>
					<g:actionSubmit value=" Show " controller="weekPlanner" action="roomView" /> 
				</g:form>
	    		<g:form class="displaySpecificInner" name="studentSearch" controller="weekPlanner" action="studentView" params="[student: studentString]">
					<g:textField name="studentString" placeholder=" person"/>
					<g:actionSubmit value=" Show " controller="weekPlanner" action="studentView"/> 
				</g:form>
	    		<g:form class="displaySpecificInner" name="subjectSearch" controller="weekPlanner" action="subjectView" params="[subject: subjectString]">
					<g:textField name="subjectString" placeholder=" subject"/>
					<g:actionSubmit value=" Show " controller="weekPlanner" action="subjectView" /> 
				</g:form>
		      </div>
		      <div class="modal-footer">
		        <button type="button" class="btn btn-default" data-dismiss="modal"><g:message code="week.planner.display.close"/></button>
		      </div>
		    </div>
		  </div>
		</div>
		<div id="squareModal" class="modal fade" role="dialog">
		  <div class="modal-dialog">
		    <!-- Modal content-->
		    <div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">Information</h4>
				</div>
				<div class="modal-body">
					<div id="squareModalBody">
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal"><g:message code="week.planner.display.close"/></button>
				</div>
		    </div>
		  </div>
		</div>
	</body>	
</html>