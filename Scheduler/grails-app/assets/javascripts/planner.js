var lastClickedId;

function revertConstraintColor(){
	document.getElementById("infoContainer").innerHTML = "";
	document.getElementById("legendOrange").style.visibility = "hidden";
	document.getElementById("legendOrangeText").style.visibility = "hidden";
	document.getElementById("legendYellow").style.visibility = "hidden";
	document.getElementById("legendYellowText").style.visibility = "hidden";
	document.getElementById("legendDarkRed").style.visibility = "hidden";
	document.getElementById("legendDarkRedText").style.visibility = "hidden";
	document.getElementById("infoContainer").style.visibility = "hidden";
	document.getElementById("legendId").style.visibility = "hidden";
	for (var i = 0; i < 5; i++){
		for (var j = 0; j < 11; j++) {
			if(j % 2 == 0) {
				dropElement = document.getElementById("locationSquare" + i + "_" + j);
				dropElement.style.backgroundColor = "white";
				dropElement = document.getElementById("teacherSquare" + i + "_" + j);
				dropElement.style.backgroundColor = "white";
				dropElement = document.getElementById("overlapSquare" + i + "_" + j);
				dropElement.style.backgroundColor = "white";
				document.getElementById("overlapSquare" + i + "_" + j).innerHTML = "";
			} else {
				dropElement = document.getElementById("locationSquare" + i + "_" + j);
				dropElement.style.backgroundColor = "#f2f0ef";
				dropElement = document.getElementById("teacherSquare" + i + "_" + j);
				dropElement.style.backgroundColor = "#f2f0ef";
				dropElement = document.getElementById("overlapSquare" + i + "_" + j);
				dropElement.style.backgroundColor = "#f2f0ef";
				document.getElementById("overlapSquare" + i + "_" + j).innerHTML = "";
			}
		}
	}
}

function dropPlanner(ev) {
    ev.preventDefault();
    var dragData = ev.dataTransfer.getData("text");
    var parentData = ev.dataTransfer.getData("parentId");
    var dropData = ev.currentTarget.getAttribute("id");

    var dragSplitted = dragData.split("_");
	var dragDay = dragSplitted[0];
	var dragStart = dragSplitted[1];
	var dragEntryId = dragSplitted[2];
    
	var dropSplitted = dropData.split("_");
	var dropDay = dropSplitted[0];
	var dropStart = dropSplitted[1];

	if (document.getElementById("teacherSquare" + dropData).style.backgroundColor == "darkred") {
		alert("Cannot swap due to teacher constraint (darkred)");
	} else if(document.getElementById("locationSquare" + dropData).style.backgroundColor == "orange"){
		findReplacementRoom(ev);
	}
	else {
		$.ajax({
			url: "http://37.139.13.59/Scheduler/weekPlanner/swapEntries",
			type: "POST",
			data:{"dragDay":dragDay, "dragStart":dragStart, "dragEntryId":dragEntryId, "dropDay":dropDay, "dropStart":dropStart},
			success: function(response) {
				document.getElementById(dropData).appendChild( document.getElementById(dragData) ) ;
				window.location.reload(true);
				}
		}); 
	}
}

function findReplacementRoom(ev) {
	ev.preventDefault();
    var dragData = ev.dataTransfer.getData("text");
    var parentData = ev.dataTransfer.getData("parentId");
    var dropData = ev.currentTarget.getAttribute("id");

    var dragSplitted = dragData.split("_");
	var dragDay = dragSplitted[0];
	var dragStart = dragSplitted[1];
	var dragEntryId = dragSplitted[2];
    
	var dropSplitted = dropData.split("_");
	var dropDay = dropSplitted[0];
	var dropStart = dropSplitted[1];

		$.ajax({
			url: "http://37.139.13.59/Scheduler/weekPlanner/roomAvailability",
			type: "GET",
			data:{"dropDay":dropDay, "dropStart":dropStart},
			success: function(response) {
				rooms = response.split("booked");
				bookedRooms = rooms[0].split("+");
				availableRooms = rooms[1].split("+");
				for (var i = 0; i < bookedRooms.length; i++) {
					for (var j = 0; j < availableRooms.length; j++) {
						if (bookedRooms[i] == availableRooms[j]) {
							availableRooms.splice(j, 1);
						} 
					}
				}
				if (availableRooms.length == 0) {
					alert("No rooms available");
				} else {	
					selectedRoom = prompt("Please select a different room from: " + availableRooms);
					for (var i = 0; i < availableRooms.length; i ++) {
						if (selectedRoom.toUpperCase() == availableRooms[i]) {
							$.ajax({
								url: "http://37.139.13.59/Scheduler/weekPlanner/changeRoom",
								type: "POST",
								data:{"roomName": availableRooms[i], "dragDay":dragDay, "dragStart":dragStart, "dragEntryId":dragEntryId, "dropDay":dropDay, "dropStart":dropStart},
								success: function(response) {
									window.location.reload();
								},
							}); 
						}
					}
				}
			},
			error: function(response) {
			alert(xhr.status);
			}
		})
}

function clickSubjectPlanner(clickedId) {

	var stringSplitted = clickedId.split("_");
	var day = stringSplitted[0];
	var start = stringSplitted[1];
	var entryId = stringSplitted[2];
	var dropElement1;
	var dropElement2;
	var counter = 0;

	revertConstraintColor();
	if(lastClickedId != clickedId) {
		$.ajax({
			url: "http://37.139.13.59/Scheduler/weekPlanner/clickSubjectPlanner",
			type: "GET",
			data:{"elementDay":day, "elementStart":start, "entryId":entryId},

			success: function(response) {
			    responseSplitted = response.split("+");
				responseLength = responseSplitted.length;
				responseMessage = responseSplitted[responseLength -1];

				for(var i = 0; i < responseLength-1; i++) {

					document.getElementById("legendId").style.visibility = "visible";
					if (responseSplitted[i] == "location") {
					 	i++;
					 	document.getElementById("legendOrange").style.visibility = "visible";
					 	document.getElementById("legendOrangeText").style.visibility = "visible";
					 	dropElement = document.getElementById("locationSquare" + responseSplitted[i]);
						dropElement.style.backgroundColor = "orange";
					} else if (responseSplitted[i] == "teacher") {
						i++;
						document.getElementById("legendDarkRed").style.visibility = "visible";
						document.getElementById("legendDarkRedText").style.visibility = "visible";
						dropElement = document.getElementById("teacherSquare" + responseSplitted[i]);
						dropElement.style.backgroundColor = "darkred";
					} else if (responseSplitted[i] == "overlap") {
						i++;
						overlap = responseSplitted[i];
						i++;
						document.getElementById("legendYellow").style.visibility = "visible";
						document.getElementById("legendYellowText").style.visibility = "visible";
						document.getElementById("infoContainer").style.visibility = "visible";
						dropElement = document.getElementById("overlapSquare" + responseSplitted[i]);
						dropElement.style.backgroundColor = "yellow";
						document.getElementById("overlapSquare" + responseSplitted[i]).innerHTML = overlap;
						}
					else if(responseSplitted[i] == "schedule"){
						i++;
						if (counter == 0){
							dropElement1 = responseSplitted[i];
							counter ++;
						}
						else {	
							counter = 0;
							var diff = responseSplitted[i].substring(1) - dropElement1.substring(1);
							for (var j = 0; j < diff; j++) {
								var x = +dropElement1.substring(1);	
								dropElement = document.getElementById("teacherSquare" + responseSplitted[i][0] + '_' + (x + j));
								dropElement.style.backgroundColor = "darkred"
							}
						} 
					}
				}
				document.getElementById("infoContainer").style.visibility = "visible";
				document.getElementById("infoContainer").innerHTML = responseMessage;
		  },
		  error:function (xhr, ajaxOptions, thrownError){
			alert(xhr.status);
		    }
		});
	}
	
	// add CSS and draggable() to element if clicked the first time
	// pass CSS and draggable() to another element if != than last clicked element
	if(clickedId != lastClickedId) {
		$( document.getElementById(clickedId) ).addClass( "subjectSelected" );		
		$( document.getElementById(clickedId) ).attr("draggable", "True");
		if ( document.getElementById(lastClickedId) != null ) {
			document.getElementById(lastClickedId).classList.remove("subjectSelected");
			$( document.getElementById(lastClickedId) ).attr("draggable", "False");
		}
		lastClickedId = clickedId;
	} else {	// if same element (clickedId == lastClickedId) is clicked again (de-select)
		document.getElementById(lastClickedId).classList.remove("subjectSelected");
		$( document.getElementById(lastClickedId) ).attr("draggable", "False");
		lastClickedId = null;
	}
}

function changeWeekPlanner(clickedValue) {
	console.log(clickedValue)
	$.ajax({
	  url: "http://37.139.13.59/Scheduler/weekPlanner/changeWeek",
	  type: "GET",
	  data:{"weekModifier":clickedValue},
	  success: function(response) {
		//  window.location.reload(true);
		window.location.href = window.location.href;
	  },
//	  error:function (xhr, ajaxOptions, thrownError){
//		alert(xhr.status);
//	    }
	});
}

function enterWeekPlanner() {
	var element = document.getElementById("enterWeekInput");
	var input = element.value;
	if(input <= 0 || input >= 53) {
		input = "numberError"
		$.ajax({
			  url: "http://37.139.13.59/Scheduler/weekPlanner/weekMessage",
			  type: "GET",
			  data:{"input":input},
			  success: function(response) {
				  window.location.reload(true);
			  },
			  error:function (xhr, ajaxOptions, thrownError){
				alert(xhr.status);
			    }
			});
	} else {
		$.ajax({
			  url: "http://37.139.13.59/Scheduler/weekPlanner/enterWeek",
			  type: "GET",
			  data:{"weekModifier":input},
			  success: function(response) {
				  window.location.reload(true);
			  },
			  error:function (xhr, ajaxOptions, thrownError){
				alert(xhr.status);
				alert(xhr.statusText);
				alert(xhr.responseText);
			    }
			});
	}
}
