function changeWeekTeacher(clickedValue) {
	$.ajax({
	  url: "http://localhost:8080/Scheduler/weekTeacher/changeWeek",
	  type: "GET",
	  data:{"weekModifier":clickedValue},
	  success: function(response) {
		  window.location.reload(true);
	  },
	  error:function (xhr, ajaxOptions, thrownError){
		alert(xhr.status);
	    }
	});
}

function enterWeekTeacher(incoming) {
	var element = document.getElementById("enterWeekInput");
	var input = element.value;
	if(input <= 0 || input >= 53) {
		input = "numberError"
		$.ajax({
			  url: "http://localhost:8080/Scheduler/weekTeacher/weekMessage",
			  type: "GET",
			  data:{"input":input},
			  success: function(response) {
				  window.location.reload(true);
			  },
			  error:function (xhr, ajaxOptions, thrownError){
				alert(xhr.status);
			    }
			});
	} else {
		$.ajax({
			  url: "http://localhost:8080/Scheduler/weekTeacher/enterWeek",
			  type: "GET",
			  data:{"weekModifier":input},
			  success: function(response) {
				  window.location.reload(true);
			  },
			  error:function (xhr, ajaxOptions, thrownError){
				alert(xhr.status);
				alert(xhr.statusText);
				alert(xhr.responseText);
			    }
			});
	}
}

function dropTeacher(ev) {
    ev.preventDefault();
    var dragData = ev.dataTransfer.getData("text");
    var dropData = ev.currentTarget.getAttribute("id");
    
    var dragSplitted = dragData.split("_");
	var dragDay = dragSplitted[0];
	var dragStart = dragSplitted[1];
    
	var dropSplitted = dropData.split("_");
	var dropDay = dropSplitted[0];
	var dropStart = dropSplitted[1];
	
	$.ajax({
		url: "http://localhost:8080/Scheduler/weekTeacher/swapEntries",
		type: "POST",
		data:{"dragDay":dragDay, "dragStart":dragStart, "dropDay":dropDay, "dropStart":dropStart},
		success: function(response) {
			document.getElementById("infoContainer").innerHTML = "Elementer blev byttet korrekt";
			window.location.reload();
		}
	});
}

function clickSubjectTeacher(clickedId) {
	var stringSplitted = clickedId.split("_");
	var day = stringSplitted[0];
	var start = stringSplitted[1];
	var entryId = stringSplitted[2];

	revertConstraintColor();
	if(lastClickedId != clickedId) {
		$.ajax({
			url: "http://localhost:8080/Scheduler/weekTeacher/clickSubjectTeacher",
			type: "GET",
			data:{"elementDay":day, "elementStart":start, "entryId":entryId},

			success: function(response) {
			    responseSplitted = response.split("+");
				responseLength = responseSplitted.length;
				responseMessage = responseSplitted[responseLength -1];

				for(var i = 1; i < responseLength-1; i++) {
					document.getElementById("legendId").style.visibility = "visible";
					if (responseSplitted[i] == "location") {
					 	i++;
					 	document.getElementById("legendOrange").style.visibility = "visible";
					 	document.getElementById("legendOrangeText").style.visibility = "visible";
					 	dropElement = document.getElementById("locationSquare" + responseSplitted[i]);
						dropElement.style.backgroundColor = "orange";
					} else if (responseSplitted[i] == "teacher") {
						i++;
						document.getElementById("legendDarkRed").style.visibility = "visible";
						document.getElementById("legendDarkRedText").style.visibility = "visible";
						dropElement = document.getElementById("teacherSquare" + responseSplitted[i]);
						dropElement.style.backgroundColor = "darkred";
					} else if (responseSplitted[i] == "overlap") {
						i++;
						overlap = responseSplitted[i];
						i++;
						document.getElementById("legendYellow").style.visibility = "visible";
						document.getElementById("legendYellowText").style.visibility = "visible";
						document.getElementById("infoContainer").style.visibility = "visible";
						dropElement = document.getElementById("overlapSquare" + responseSplitted[i]);
						dropElement.style.backgroundColor = "yellow";
						document.getElementById("overlapSquare" + responseSplitted[i]).innerHTML = overlap;
						} 
				}
				document.getElementById("infoContainer").style.visibility = "visible";
				document.getElementById("infoContainer").innerHTML = responseMessage;
		  },
		  error:function (xhr, ajaxOptions, thrownError){
			alert(xhr.status);
		    }
		});
	}
	
	// add CSS and draggable() to element if clicked the first time
	// pass CSS and draggable() to another element if != than last clicked element
	if(clickedId != lastClickedId) {
		$( document.getElementById(clickedId) ).addClass( "subjectSelected" );		
		$( document.getElementById(clickedId) ).attr("draggable", "True");
		if ( document.getElementById(lastClickedId) != null ) {
			document.getElementById(lastClickedId).classList.remove("subjectSelected");
			$( document.getElementById(lastClickedId) ).attr("draggable", "False");
		}
		lastClickedId = clickedId;
	} else {	// if same element (clickedId == lastClickedId) is clicked again (de-select)
		document.getElementById(lastClickedId).classList.remove("subjectSelected");
		$( document.getElementById(lastClickedId) ).attr("draggable", "False");
		lastClickedId = null;
	}
}

