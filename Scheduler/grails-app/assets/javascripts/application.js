// This is a manifest file that'll be compiled into application.js.
//
// Any JavaScript file within this directory can be referenced here using a relative path.
//
// You're free to add application-wide JavaScript to this file, but it's generally better 
// to create separate JavaScript files as needed.
//

//= require jquery
//= require jquery-ui
//= require bootstrap
//= require planner
//= require teacher
//= require_self
//= require_tree .

if (typeof jQuery !== 'undefined') {
	(function($) {
		$(document).ajaxStart(function(){
			$('#spinner').fadeIn();
		}).ajaxStop(function(){
			$('#spinner').fadeOut();
		});
	})(jQuery);
}

function allowDrop(ev) {
    ev.preventDefault();
}

function drag(ev) {
    ev.dataTransfer.setData("text", ev.target.id);    
}

function dropTableNow(tableId) {
	$.ajax({
		url: "http://37.139.13.59/Scheduler/dropTableNow/selectTable",
		type: "GET",
		data:{"tableId":tableId},
		success: function(response) {
			window.location.reload(true);
		}
	});
}



