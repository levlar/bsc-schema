package scheduler

class WeekTeacherController {
	EntryAccessService entryAccessService
	WeekSwapService weekSwapService
	SchemaEntryService schemaEntryService

	def date = new Date().clearTime()
	int currentWeek =  date[Calendar.WEEK_OF_YEAR] -1
	// "globals" for clickSubjectTeacher()
	SchemaEntry lookupEntry
	Room lookupRoom
	def equalRooms
	Subject lookupSubject
	def overlapStudentsArray
	def listePlanner
	
	//Globals for different views
	int view = 0;
	String studentName
	String roomName
	
    def index() {		
		def nuListe = entryAccessService.selectWeekQuery(currentWeek)
		return [queryResult : nuListe, nuWeek:currentWeek]
	}
	
	def roomView(params) {
		roomName = params.roomString.capitalize()
		view = 2
		listePlanner = entryAccessService.selectRoomQuery(currentWeek, roomName);
		if (listePlanner == null) {
			println "ROOMS == NUILL"
		} else {
			render(view: "index", model: [queryResult : listePlanner, nuWeek:currentWeek])
		}
	}

	def studentView(params) {
		view = 3
		studentName = params.studentString;
		listePlanner = entryAccessService.selectStudentQuery(currentWeek, studentName);
		if (listePlanner == null) {
			println "STUDENTS == NUILL"
		} else {
			render(view: "index", model: [queryResult : listePlanner, nuWeek:currentWeek])
		}
	}

	def changeWeek(int weekModifier) {
		currentWeek += weekModifier
		def nuListe = entryAccessService.selectWeekQuery(currentWeek)
		render(view:"index", model:[queryResult:nuListe, nuWeek:currentWeek])
	}

	def swapEntries(int dragDay, int dragStart, int dropDay, int dropStart) {
		def nuListe = weekSwapService.swapEntries(currentWeek, dragDay, dragStart, dropDay, dropStart)
		render(view:"index", model:[queryResult:nuListe, nuWeek:currentWeek])
	}
	
	def clickSubjectTeacher(int elementDay, int elementStart, Long entryId) {
		int[][] amountOfOverlap = new int[5][11]
		boolean isNull = false;
		try{	
			lookupEntry = schemaEntryService.lookupAnEntry(elementDay, elementStart, entryId)
		} catch(NullPointerException) {
			println("ENTRY, check Json files have been uploaded");
		}
		try{	
			lookupSubject = schemaEntryService.lookupSubject(lookupEntry.title)
		} catch(NullPointerException) {
			println("SUBJECT, check Json files have been uploaded");
		}
		try{	
			equalRooms = schemaEntryService.lookupRoom(lookupEntry.location)
		} catch(NullPointerException) {
			println("ROOMS, check Json files have been uploaded");
		}
		try {
			overlapStudentsArray = schemaEntryService.lookupStudents(lookupSubject.studentsEnrolled)
		} catch(NullPointerException) {
			isNull = true;
			println("STUDENTS, check Json files have been uploaded");
		}		
		if (isNull == false){
			listePlanner.each { listeIt ->
				overlapStudentsArray.each { overlapIt ->
				 	if (overlapIt != null) {
						def splittedInput = overlapIt.studentSubjects.split(",")
						splittedInput.each { subjectListe ->	
							if(subjectListe == listeIt.title) { 
								amountOfOverlap[listeIt.day][listeIt.start] ++
							} 
						}
					} 
				} 
			}
		}	
		String result = "+"
		String tempSpecificConstraint = ""
		def roomSet = false
		def teacherSet = false
		def overlapSet = false
		listePlanner.each {
			if(lookupEntry.location == it.location && lookupEntry.id != it.id ) {
				result += "location"
				result += "+"
				result += it.day + "_" + it.start
				result += "+"
				if(roomSet == false) {
					tempSpecificConstraint += message(code: 'legend.info.room')
					roomSet = true
				}				
			} 
			if (it.teacher == lookupEntry.teacher && it.teacher != "") {
				result += "teacher"
				result += "+"
				result += it.day + "_" + it.start
				result += "+"
				if(teacherSet == false) {					
					tempSpecificConstraint += message(code: 'legend.info.teacher')
					teacherSet = true
				}
			}
			if(amountOfOverlap[it.day][it.start] > 0) {
				result += "overlap"
				result += "+"
				result += amountOfOverlap[it.day][it.start]
				result += "+"
				result += it.day + "_" + it.start
				result += "+"
				if(overlapSet == false) {
					tempSpecificConstraint += message(code: 'legend.info.overlap')
					overlapSet = true
				}
			}
		}
		result += tempSpecificConstraint
		render(result)
	}
	
	def enterWeek(int weekModifier) {
		currentWeek = weekModifier
		def listePlanner = entryAccessService.selectWeekQuery(currentWeek)
		render(view:"index", model:[queryResult:listePlanner, nuWeek:currentWeek])
	}
	
	def weekMessage(String input) {
		switch(input) {
			case "numberError":
				flash.message = message(code: 'error.number.input')
				render(view:"/index")
				break
		}
	}
}
