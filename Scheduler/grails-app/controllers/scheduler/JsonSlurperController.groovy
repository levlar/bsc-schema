package scheduler

class JsonSlurperController {
	
	JsonSlurperService jsonSlurperService;
	JsonStreamService jsonStreamService;

    def index() {
	}
	
	def uploadStudents() {	
		jsonStreamService.uploadJson(params)
		def result = jsonSlurperService.inputStudents()
		if(result == 1){
			flash.message = message(code: 'upload.empty')
			render(view:"../index")
		} else {
			flash.message = message(code: 'upload.student')
			render(view:"../index")
		}
	}
	
	def uploadRooms() {	
		jsonStreamService.uploadJson(params)
		def result = jsonSlurperService.inputRooms()
		if(result == 1){
			flash.message = message(code: 'upload.empty')
			render(view:"../index")
		} else {
			flash.message = message(code: 'upload.room')
			render(view:"../index")
		}
	}
	
	def uploadSubjects() {
		jsonStreamService.uploadJson(params)
		def result = jsonSlurperService.inputSubjects()
		if(result == 1){
			flash.message = message(code: 'upload.empty')
			render(view:"../index")
		} else {
			flash.message = message(code: 'upload.subject')
			render(view:"../index")
		}
	}
	
	def uploadSchemaEntries() {
		jsonStreamService.uploadJson(params)
		def result = jsonSlurperService.inputSchemaEntries()
		if(result == 1){
			flash.message = message(code: 'upload.empty')
			render(view:"../index")
		} else {
			flash.message = message(code: 'upload.schema')
			render(view:"../index")
		}
	}
}
