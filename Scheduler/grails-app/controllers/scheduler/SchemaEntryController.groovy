package scheduler

import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Transactional(readOnly = true)
class SchemaEntryController {
    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond SchemaEntry.list(params), model:[schemaEntryInstanceCount: SchemaEntry.count()]
    }

    def show(SchemaEntry schemaEntryInstance) {
        respond schemaEntryInstance
    }

    def create() {
        respond new SchemaEntry(params)
    }

    @Transactional
    def save(SchemaEntry schemaEntryInstance) {
        if (schemaEntryInstance == null) {
            notFound()
            return
        }
        if (schemaEntryInstance.hasErrors()) {
            respond schemaEntryInstance.errors, view:'create'
            return
        }
        schemaEntryInstance.save flush:true
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'schemaEntry.label', default: 'SchemaEntry'), schemaEntryInstance.id])
                redirect schemaEntryInstance
            }
            '*' { respond schemaEntryInstance, [status: CREATED] }
        }
    }

    def edit(SchemaEntry schemaEntryInstance) {
        respond schemaEntryInstance
    }

    @Transactional
    def update(SchemaEntry schemaEntryInstance) {
        if (schemaEntryInstance == null) {
            notFound()
            return
        }
        if (schemaEntryInstance.hasErrors()) {
            respond schemaEntryInstance.errors, view:'edit'
            return
        }
        schemaEntryInstance.save flush:true
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'SchemaEntry.label', default: 'SchemaEntry'), schemaEntryInstance.id])
                redirect schemaEntryInstance
            }
            '*'{ respond schemaEntryInstance, [status: OK] }
        }
    }

    @Transactional
    def delete(SchemaEntry schemaEntryInstance) {
        if (schemaEntryInstance == null) {
            notFound()
            return
        }
        schemaEntryInstance.delete flush:true
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'SchemaEntry.label', default: 'SchemaEntry'), schemaEntryInstance.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'schemaEntry.label', default: 'SchemaEntry'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
