package scheduler

class WeekPlannerController {
	EntryAccessService entryAccessService
	WeekSwapService weekSwapService
	PlannerSwapService plannerSwapService
	SchemaEntryService schemaEntryService
	StringBuilderService stringBuilderService
	ExportService exportService

	int currentWeek = Calendar.getInstance().get(Calendar.WEEK_OF_YEAR)-1
	int currentYear = Calendar.getInstance().get(Calendar.YEAR)
	// "globals" for clickSubjectPlanner() and dragStartCompare()
	SchemaEntry lookupEntry
	Room lookupRoom
	def equalRooms
	Subject lookupSubject
	def overlapStudentsArray
	def listePlanner
	def busySchedule
	//Globals for different views
	int view = 0;
	def studentName 
	def roomName
	def subjectName
	
    def index() {
		flash.clear()
    	view = 1;
		listePlanner = entryAccessService.selectPlannerWeek(currentWeek)
		return [queryResult:listePlanner, nuWeek:currentWeek, nuYear:currentYear]
	}

	def roomView(params) {
		view = 2
		if(params.roomString != null) {
			roomName = params.roomString
		} else if (params.viewString != null)  {
			roomName = params.viewString
		}
		listePlanner = entryAccessService.selectRoomQuery(currentWeek, roomName);
		if (listePlanner == null) {
			println "ROOMS == NUILL" 
		} else {
			render(view:"index", model: [queryResult:listePlanner, nuWeek:currentWeek, viewString:roomName])
		}
	}

	def studentView(params) {
		view = 3
		if (params.studentString != null) {
			studentName = params.studentString;
		} else if(params.viewString != null){
			studentName = params.viewString
		}
		listePlanner = entryAccessService.selectStudentQuery(currentWeek, studentName);
		if (listePlanner == null) {
			println "STUDENTS == NUILL" 
		} else {
			render(view:"index", model: [queryResult:listePlanner, nuWeek:currentWeek, viewString:studentName])
		}
	}
	
	def subjectView(params) {
		view = 4
		if(params.subjectString != null) {
			subjectName = params.subjectString
		} else if (params.viewString != null)  {
			subjectName = params.viewString
		}
		listePlanner = entryAccessService.selectSubjectQuery(currentWeek, subjectName);
		if (listePlanner == null) {
			println "ROOMS == NUILL"
		} else {
			render(view:"index", model: [queryResult:listePlanner, nuWeek:currentWeek, viewString:subjectName])
		}
	}
	
	def changeWeek(int weekModifier) {
		currentWeek += weekModifier
		switch (view) {
			case 1:
				listePlanner = entryAccessService.selectWeekQuery(currentWeek)
				break;
			case 2:
				listePlanner = entryAccessService.selectRoomQuery(currentWeek, roomName)
				break;
			case 3:
				listePlanner = entryAccessService.selectStudentQuery(currentWeek, studentName)
				break;
			case 4:
				listePlanner = entryAccessService.selectSubjectQuery(currentWeek, subjectName)
			}
		render(view:"index", model:[queryResult:listePlanner, nuWeek:currentWeek])	
	}
	
	def enterWeek(int weekModifier) {
		currentWeek = weekModifier
		listePlanner = entryAccessService.selectWeekQuery(currentWeek)
		render(view:"index", model:[queryResult:listePlanner, nuWeek:currentWeek])
	}

	def swapEntries(int dragDay, int dragStart, Long dragEntryId, int dropDay, int dropStart) {
		plannerSwapService.swapEntries(currentWeek, dragDay, dragStart, dragEntryId, dropDay, dropStart)
		render(view:"index"); 
	}

	def changeRoom(String roomName, int dragDay, int dragStart, Long dragEntryId, int dropDay, int dropStart){
		plannerSwapService.newRoom(currentWeek, roomName, dragDay, dragStart, dragEntryId);
		swapEntries(dragDay, dragStart, dragEntryId, dropDay, dropStart);
	}

	def roomAvailability(int dropDay, int dropStart) {
		def rooms = schemaEntryService.bookedRooms(dropDay, dropStart)
		String result = "+"
		rooms.each{
			result += it.location
			result += "+"
		}
		result += "booked"
		result += returnRooms();
		render(result);
	}

	def returnRooms() {
		String result = "+";
		equalRooms.each {
			result += it.roomName;
			result += "+";
		}
		return result;
	}

	def export() {
		exportService.writeToJson()
		render(view:"../index")
	}

	def clickSubjectPlanner(int elementDay, int elementStart, Long entryId) {
		def listePlanner2 = entryAccessService.selectWeekQuery(currentWeek);
		try{	
			lookupEntry = schemaEntryService.lookupAnEntry(elementDay, elementStart, entryId)
		} catch(NullPointerException) {
			println("ENTRY, check Json files have been uploaded");
		}
		try{	

			lookupSubject = schemaEntryService.lookupSubject(lookupEntry.title)
		} catch(NullPointerException) {
			println("SUBJECT, check Json files have been uploaded");
		}
		try{	
			equalRooms = schemaEntryService.lookupRoom(lookupEntry.location)
		} catch(NullPointerException) {
			println("ROOMS, check Json files have been uploaded, or no equal rooms found");
		}
		try {
			overlapStudentsArray = schemaEntryService.lookupStudents(lookupSubject.studentsEnrolled)

		} catch(NullPointerException) {
			println("STUDENTS, check Json files have been uploaded");
		}
		try{
			//Resetting the schedule string
			busySchedule = "";
			busySchedule = schemaEntryService.teacherSchedule(lookupSubject.subjectTeacher,currentWeek)
		}	
		catch(NullPointerException) {
			println ("No teacher found")
		}
		String result = stringBuilderService.result(listePlanner2, lookupEntry, busySchedule.toString(), overlapStudentsArray)
		render(result)
	}
	
	def weekMessage(String input) {
		switch(input) {
			case "numberError":
				flash.message = message(code: 'error.number.input')
				render(view:"/index")
				break
		}
	}
}
