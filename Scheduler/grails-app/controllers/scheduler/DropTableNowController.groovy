package scheduler

class DropTableNowController {
	DropTableService dropTableService

    def index() { }
	
	def selectTable(String tableId){
		dropTableService.dropTableNow(tableId)
		flash.message = message(code: 'clear.database')
		render(view:"../index")
	}
}
