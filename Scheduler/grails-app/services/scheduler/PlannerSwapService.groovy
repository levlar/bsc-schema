package scheduler

import grails.transaction.Transactional

@Transactional
class PlannerSwapService {

    def serviceMethod() {
    }
	
	def swapEntries(int week, int dragDay, int dragStart, Long dragEntryId, int dropDay, int dropStart) {
		try {
			SchemaEntry dragEntry = SchemaEntry.find("from SchemaEntry where week = :week and day = :day and start = :start and id = :dragEntryId",
				[week:week, day:dragDay, start:dragStart, dragEntryId:dragEntryId])
			
			dragEntry.time = 8 + dropStart + ":00-" + (9 + dropStart) + ":00"
			dragEntry.day = dropDay
			dragEntry.start = dropStart
			dragEntry.save(flush: true)
		} catch(org.hibernate.QueryParameterException e) {
			println "Fejl i DBaccessService.swapEntries..."
		}
	}

	def newRoom(int week, String roomName, int dragDay, int dragStart, Long dragEntryId){
		SchemaEntry dragEntry = SchemaEntry.find("from SchemaEntry where week = :week and day = :day and start = :start and id = :dragEntryId",
				[week:week, day:dragDay, start:dragStart, dragEntryId:dragEntryId])
		dragEntry.location = roomName
		dragEntry.save(flush:true);
	}
}
