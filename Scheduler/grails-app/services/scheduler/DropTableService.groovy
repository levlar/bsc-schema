package scheduler

import grails.transaction.Transactional

@Transactional
class DropTableService {

    def serviceMethod() {
    }

	def dropTableNow(String tableId) {
		switch(tableId) {
			case "SchemaEntry":
				try {
					SchemaEntry.executeUpdate('delete from SchemaEntry')
				} catch (Throwable t) {
					throw t;
				}
				break
			case "Student":
				try {
					Student.executeUpdate('delete from Student')
				} catch (Throwable t) {
					throw t;
				}
				break
			case "Room":
				try {
					Room.executeUpdate('delete from Room')
				} catch (Throwable t) {
					throw t;
				}
				break
			case "Subject":
				try {
					Subject.executeUpdate('delete from Subject')
				} catch (Throwable t) {
					throw t;
				}
				break
		}
	}
}
