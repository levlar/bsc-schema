package scheduler

import grails.transaction.Transactional

@Transactional
class ConstructInputObjectsService {

    def serviceMethod() {
    }

	def createSchemaEntry(Object result){
		try {
			result.each {
				int i = 0;
				int year = 2016;
				//Cuts the string to get both the full string and the week number
				String aux = it.toString().substring(0, 2)
				int week = it.toString().substring(0,2).toInteger()

				for(i = 0; i < result.get(aux).size(); i++ ) {
					SchemaEntry schemaEntry = new SchemaEntry(week, result.get(aux)[i].id.toInteger(), result.get(aux)[i].day.toInteger(),
															  result.get(aux)[i].start.toInteger(), result.get(aux)[i].duration.toInteger(),
															  year, result.get(aux)[i].title,
															  result.get(aux)[i].location, result.get(aux)[i].color,
															  result.get(aux)[i].time);
					if (schemaEntry.duration > 1) {
						int duration = schemaEntry.duration;
						schemaEntry.duration = 1
						for (int q = 0; q < duration - 1; q++) {
							SchemaEntry schemaEntryDur = new SchemaEntry(week, result.get(aux)[i].id.toInteger(), result.get(aux)[i].day.toInteger(),
															  result.get(aux)[i].start.toInteger() + q + 1, 1,
															  year, result.get(aux)[i].title,
															  result.get(aux)[i].location, result.get(aux)[i].color,
															  result.get(aux)[i].time,
															 // 8 + result.get(aux)[i].start + (q+1) + ":00-" + (9 + result.get(aux)[i].start + (q+1)) + ":00",
															  );
							schemaEntryDur.save(flush: true);
						}
					}
					schemaEntry.save(flush: true)
				}
			}
		} catch (groovy.lang.GroovyRuntimeException e) {
		}
	}
	
	def createStudents(Object result){
		try {
			result.persons.each {
				Student student = new Student(it.name, it.enrollments, it.type)
				student.busy = it.busy
				student.save(flush:true)
				}
			} catch (groovy.lang.GroovyRuntimeException e) {
		}
	}
	
	def createRooms(Object result){
		try {
			result.general.each {
					Room room = new Room(it.roomName, it.maxAllowed.toInteger(), it.projector.toBoolean(),
									it.whiteboard.toBoolean(), it.tvScreens.toBoolean(), it.speakerSystem.toBoolean())
					room.save(flush: true)
				}
			} catch (groovy.lang.GroovyRuntimeException e) {
		}
	}

	def createSubjects(Object result){
		try {
			result.imada.each {
					Subject subject = new Subject(it.subjectName,
						 it.subjectTeacher,
						 it.studentsEnrolled,
						 it.subjectWeekStart.toInteger(),
						 it.subjectWeekEnd.toInteger(),
						 it.subjectYear.toInteger())

					subject.save(flush: true)
				}
			} catch (groovy.lang.GroovyRuntimeException e) {
		}
	}
}
