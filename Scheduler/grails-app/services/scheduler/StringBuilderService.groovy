package scheduler

import grails.transaction.Transactional

@Transactional
class StringBuilderService {
	I18nService i18nService

    def serviceMethod() {
    }
	
	def findSchedule(Student teacher, int currentWeek) {
		String schedule = "";
    	def splittedString = teacher.busy.replaceAll("[\\[\\]']","");
    	splittedString = splittedString.replaceAll("\\s", "");
    	splittedString = splittedString.split(',')
    	splittedString.each{
       		def week = it.substring(1,3);
    		def day = it.substring(4,5);
    		def time = it.substring(6)
    		if (week.toInteger() == currentWeek) {
    			schedule += "schedule"
       			schedule += "+"
				switch(day) {
	    			case 'M':
	    				day = 0;    				
	    				break;
	    			case 'T':
	    				day = 1;
	    				break;
	    			case 'W':
	    				day = 2;
	    				break;
	    			case 'H':
	    				day = 3;
	    				break;
	    			case 'F':
	    				day = 4
	    				break;
	    		}
	    		schedule += day;
	    		schedule += time.toInteger()-8;
	    		schedule += "+"
	    	}
    	}
    	return schedule;
	}

	def calculateOverlap(def listePlanner, def overlapStudentsArray) {
		int[][] amountOfOverlap = new int[5][11]
		listePlanner.each { listeIt ->
			overlapStudentsArray.each { overlapIt ->
			 	if (overlapIt != null) {
					def splittedInput = overlapIt.studentSubjects.split(",")
					splittedInput.each { subjectListe ->	
						if(subjectListe == listeIt.title) { 
							amountOfOverlap[listeIt.day][listeIt.start] ++
						} 
					}
				} 
			} 
		}
		return amountOfOverlap
	}
	
	def result(def listePlanner, SchemaEntry lookupEntry, String busySchedule, def overlapStudentsArray) {
		String result = busySchedule
		int[][] amountOfOverlap = new int[5][11]
		amountOfOverlap = calculateOverlap(listePlanner, overlapStudentsArray)
		String tempSpecificConstraint = ""
		def roomSet = false
		def teacherSet = false
		def overlapSet = false		
		listePlanner.each {
			if(lookupEntry.location == it.location && lookupEntry.id != it.id ) {
				result += "location"
				result += "+"
				result += it.day + "_" + it.start
				result += "+"
				if(roomSet == false) {
					tempSpecificConstraint += i18nService.msg('legend.info.room')
					roomSet = true
				}				
			} 
			Subject databaseSubject = Subject.find("from Subject where subjectName = :title",[title:lookupEntry.title])
			Subject entrySubject = Subject.find("from Subject where subjectName = :title",[title:it.title])
			if (entrySubject.subjectTeacher == databaseSubject.subjectTeacher && entrySubject != null && lookupEntry.id != it.id) {
				result += "teacher"
				result += "+"
				result += it.day + "_" + it.start
				result += "+"
				if(teacherSet == false) {					
					tempSpecificConstraint += i18nService.msg('legend.info.teacher')
					teacherSet = true
				}
			}
			if(amountOfOverlap[it.day][it.start] > 0 && lookupEntry.id != it.id) {
				result += "overlap"
				result += "+"
				result += amountOfOverlap[it.day][it.start]
				result += "+"
				result += it.day + "_" + it.start
				result += "+"
				if(overlapSet == false) {
					tempSpecificConstraint += i18nService.msg('legend.info.overlap')
					overlapSet = true
				}
			} 
		}
		result += tempSpecificConstraint
		return result;  
	} 
}
	