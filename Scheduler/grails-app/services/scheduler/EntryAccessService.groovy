package scheduler

import grails.transaction.Transactional

@Transactional
class EntryAccessService {

    def serviceMethod() {
    }
	
	def selectWeekQuery(int week){
		def nuList = SchemaEntry.findAll("from SchemaEntry where week = ? order by start, day",[week])
		return nuList;
	}

	def selectPlannerWeek(int week){
		def nuList = SchemaEntry.findAll("from SchemaEntry where week = ? order by start, day", [week]);
		return nuList;
	}

	def selectRoomQuery(int week, String room) {
		def nuList = SchemaEntry.findAll("from SchemaEntry where week = ? and location = ? order by start, day", [week,room]);
		return nuList;
	}
	
	def selectSubjectQuery(int week, String subject) {
		def nuList = SchemaEntry.findAll("from SchemaEntry where week = ? and title = ? order by start, day", [week,subject]);
		return nuList;
	}

	def selectStudentQuery(int week, String studentName) {
		def student = Student.find("from Student where studentName = ?", [studentName]);
		try { 
			def splittedInput = student.studentSubjects.split(",")
			def nuList = new SchemaEntry[splittedInput.size()];
			int i = 0;
			splittedInput.each{
 				nuList[i] = SchemaEntry.find("from SchemaEntry where week = ?  and title = ?", [week, it]);
				if(nuList[i] != null) {
					i++
				}
			}
	 		def tempList = []
	 		int counter = 0;
	 		for (i = 0; i < nuList.size(); i++) {
		 		if (nuList[counter] != null) {
		 			tempList[counter] = nuList[i]
		 			counter ++;
		 		}
		 	}	
			return tempList;
		} catch(NullPointerException) {
			//flash.message = "Oops.. Something went wrong"
			println"SOMETHING WENT WRONG"
			} 
	} 
}
