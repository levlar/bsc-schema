package scheduler

import grails.transaction.Transactional

@Transactional
class SchemaEntryService {

	StringBuilderService stringBuilderService

    def serviceMethod() {
    }
	
    def teacherSchedule(String teacherName, int currentWeek) {
    	Student teacher = Student.find("from Student where studentName = ?", [teacherName])
    	return stringBuilderService.findSchedule(teacher, currentWeek)
    }

	def lookupAnEntry(int day, int start, Long entryId) {
		SchemaEntry lookupEntry = SchemaEntry.find("from SchemaEntry where day = :day and start = :start and id = :entryId", [day:day, start:start, entryId:entryId])
		return lookupEntry
	}
	
	def bookedRooms(int dropDay, int dropStart){
		def rooms = SchemaEntry.findAll("from SchemaEntry where day = :day and start = :start", [day:dropDay, start:dropStart]);
		return rooms;
	}

	def lookupRoom(String roomName) {
		Room lookupRoom = Room.find("from Room where roomName = :roomName",	[roomName:roomName])
		def rooms = Room.findAll("from Room where projector >= :projector and whiteboard >= :whiteboard and tvScreens >= :tvScreens and speakerSystem >= :speakerSystem and maxAllowed >= :numberOfStudents and roomName != :roomName order by maxAllowed asc",
				[projector:lookupRoom.projector,whiteboard:lookupRoom.whiteboard, tvScreens:lookupRoom.tvScreens, speakerSystem:lookupRoom.speakerSystem, numberOfStudents:lookupRoom.maxAllowed, roomName: lookupRoom.roomName])
		return rooms
	}
	
	def lookupSubject(String subjectTitle) {
		Subject lookupSubject = Subject.find("from Subject where subjectName = :subjectTitle", [subjectTitle:subjectTitle])
		return lookupSubject
	}
	
	def lookupStudents(String studentsEnrolled) {
		def listStudents = studentsEnrolled.split(",")
		def studentList = new Student[1000] 
		for (int i = 0; i < listStudents.size(); i++) {
			Student student = Student.find("from Student where studentName = ?", [listStudents[i]])
			studentList[i] = student
		}
		return studentList
	}
}
