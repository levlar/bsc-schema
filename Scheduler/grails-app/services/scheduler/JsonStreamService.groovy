package scheduler

import grails.transaction.Transactional

@Transactional
class JsonStreamService {

    def uploadJson(params) {
		if(params.cfile && params.size() > 0) {
			if(params.cfile instanceof org.springframework.web.multipart.commons.CommonsMultipartFile) {
				new FileOutputStream('temp.json').leftShift( params.cfile.getInputStream() );
				}
			else {
				log.error("wrong attachment type [${cfile.getClass()}]");
			}
		}
		else {
		}
    }
}
