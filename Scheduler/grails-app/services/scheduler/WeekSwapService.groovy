package scheduler

import grails.transaction.Transactional

@Transactional
class WeekSwapService {

    def serviceMethod() {

    }
	def swapEntries(int week, int dragDay, int dragStart, int dropDay, int dropStart) {
		String tempTime;

		try {
			SchemaEntry dragEntry = SchemaEntry.find("from SchemaEntry where week = :week and day = :day and start = :start",
				[week:week, day:dragDay, start:dragStart])
			SchemaEntry dropEntry = SchemaEntry.find("from SchemaEntry where week = :week and day = :day and start = :start",
				[week:week, day:dropDay, start:dropStart])
			
			if(dropEntry == null) {
				dragEntry.day = dropDay
				dragEntry.start = dropStart
				dragEntry.time = 8 + dropStart + ":00-" + (9 + dropStart) + ":00"  
				dragEntry.save(flush:true);
			} else {
				dropEntry.day = dragDay
				dropEntry.start = dragStart
				tempTime = dragEntry.time;
				dragEntry.time = dropEntry.time;
				dropEntry.time = tempTime;
				dragEntry.day = dropDay
				dragEntry.start = dropStart

				dragEntry.save(flush: true)
				dropEntry.save(flush: true)
			}

		} catch(org.hibernate.QueryParameterException e) {
			println "Fejl i DBaccessService.swapEntries..."
		}
	}
}
