package scheduler

import org.springframework.web.servlet.i18n.SessionLocaleResolver
import org.springframework.context.MessageSource
import org.codehaus.groovy.grails.web.servlet.mvc.GrailsWebRequest
import org.codehaus.groovy.grails.web.util.WebUtils

class I18nService {
 
    boolean transactional = false
    SessionLocaleResolver localeResolver
    MessageSource messageSource
    /**
     * @param msgKey
     * @param defaultMessage default message to use if none is defined in the message source
     * @param objs objects for use in the message
     * @return
     */
    def msg(String msgKey, String defaultMessage = null, List objs = null) {
        GrailsWebRequest webUtils = WebUtils.retrieveGrailsWebRequest()
        def request = webUtils.getCurrentRequest()
        def msg = messageSource.getMessage(msgKey,objs?.toArray(),defaultMessage,localeResolver.resolveLocale(request))
        if (msg == null || msg == defaultMessage) {
            log.warn("No i18n messages specified for msgKey: ${msgKey}")
            msg = defaultMessage
        }
        return msg
    }
    /**
     * Method to look like g.message
     * @param args
     * @return
     */
    def message(Map args) {
        return msg(args.code, args.default, args.attrs)
    }
}