package scheduler

import grails.transaction.Transactional
import groovy.json.JsonSlurper;

@Transactional
class JsonSlurperService {
	ConstructInputObjectsService constructInputObjectsService
	def slurper = new JsonSlurper();
    int inputSchemaEntries(){
		File json_in = new File("temp.json")
		def jsonText = json_in.getText()
		try {
			def result = slurper.parseText(jsonText)			
			constructInputObjectsService.createSchemaEntry(result)
			boolean fileSuccessfullyDeleted =  new File("temp.json").delete()
			return 0
		}
		catch(IllegalArgumentException) {
			return 1
		}
    }
	
	int inputStudents(){
		File json_in = new File("temp.json")
		def jsonText = json_in.getText()
		try {
			def result = slurper.parseText(jsonText)
			constructInputObjectsService.createStudents(result)
			boolean fileSuccessfullyDeleted =  new File("temp.json").delete()
			return 0
		}
		catch(IllegalArgumentException) {
			return 1
		}
	}
	
	int inputRooms(){
		File json_in = new File("temp.json")
		def jsonText = json_in.getText()
		try {
			def result = slurper.parseText(jsonText)
			constructInputObjectsService.createRooms(result)
			boolean fileSuccessfullyDeleted =  new File("temp.json").delete()
			return 0
		}
		catch(IllegalArgumentException) {
			return 1
		}
	}
	
	int inputSubjects(){
		File json_in = new File("temp.json")
		def jsonText = json_in.getText()
		try {
			def result = slurper.parseText(jsonText)
			constructInputObjectsService.createSubjects(result)
			boolean fileSuccessfullyDeleted =  new File("temp.json").delete()
			return 0
		}
		catch(IllegalArgumentException) {
			return 1
		}
	}
}
