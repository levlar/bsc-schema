package scheduler

import grails.transaction.Transactional

@Transactional
class ExportService {

    def writeToJson(){
    	def listePlanner = SchemaEntry.findAll("from SchemaEntry order by week");
    	File output = new File("output.json")
        int counter = 0
    	int week = listePlanner[0].week
    	output.write("{ \"${week}\" : [ \n")
    	listePlanner.each {
    		counter ++
    		if (  week != it.week ) {
                week = it.week
                output << "],\n"
    			output << "\"${week}\" : [ \n"
    		}
            if (counter == listePlanner.size()) {
                output << "{ \n"
                output << "\"id\":\""+ it.id +"\",\n"
                output << "\"day\":\""+ it.day +"\",\n"
                output << "\"start\":\""+ it.start +"\",\n"
                output << "\"duration\":\""+ it.duration +"\",\n"
                output << "\"title\":\""+ it.title +"\",\n"
                output << "\"color\":\""+ it.color +"\",\n"
                output << "\"time\":\""+ it.time +"\",\n"
                output << "\"location\":\""+ it.location +"\",\n"
                output << "}\n"
                output << "]}\n"
            }
            else {
                output << "{ \n"
                output << "\"id\":\""+ it.id +"\",\n"
                output << "\"day\":\""+ it.day +"\",\n"
                output << "\"start\":\""+ it.start +"\",\n"
                output << "\"duration\":\""+ it.duration +"\",\n"
                output << "\"title\":\""+ it.title +"\",\n"
                output << "\"color\":\""+ it.color +"\",\n"
                output << "\"time\":\""+ it.time +"\",\n"
                output << "\"location\":\""+ it.location +"\",\n"
                if( listePlanner[counter] != null && listePlanner[counter].week != week) {
                    output << "}\n"
                } else {
                    output << "},\n"
                }
            }
    	}
    }
}