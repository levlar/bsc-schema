package scheduler

class Subject {
	String subjectName
	String subjectTeacher
	String studentsEnrolled
	int subjectWeekStart
	int subjectWeekEnd
	int subjectYear
		
	Subject(String subjectName,	String subjectTeacher, String studentsEnrolled, int subjectWeekStart,	int subjectWeekEnd,
					int subjectYear){
						this.subjectName = subjectName
						this.subjectTeacher = subjectTeacher
						this.studentsEnrolled = studentsEnrolled
						this.subjectWeekStart = subjectWeekStart
						this.subjectWeekEnd = subjectWeekEnd
						this.subjectYear = subjectYear
					}

    static constraints = {
		subjectName			blank : false
		subjectTeacher		blank : false
		studentsEnrolled	blank : false, size:0..10000
		subjectWeekStart	min : 0
		subjectWeekEnd		min : 0
		subjectYear			min : 0
	}
}
