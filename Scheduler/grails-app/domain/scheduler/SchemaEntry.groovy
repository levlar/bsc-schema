package scheduler

class SchemaEntry {
	int week, entryId, day, start, duration, year, overlap = 0
	String title, location, color, time
		
	SchemaEntry(int week, int entryId, int day, int start, int duration, int year,
				String title, String location, String color, String time){
				 
		this.week = week
		this.entryId = entryId
		this.day = day
		this.start = start
		this.duration = duration
		this.year = year
		this.title = title
		this.location = location
		this.color = color
		this.time = time
	}	

    static constraints = {
    	week		min: -1
    	entryId		min: -1
    	day			min: -1
    	start		min: -1
    	duration	min: -1
		year		min: -1
		title 		nullable: false
		location 	nullable: false
		color 		nullable: false
		time 		nullable: false
    }
}
