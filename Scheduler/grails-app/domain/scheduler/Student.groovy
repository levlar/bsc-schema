package scheduler

class Student {
	String studentName
	String studentSubjects
	String type
	String busy
	
	Student(String studentName, String studentSubjects, String type) {
		this.studentName = studentName
		this.studentSubjects = studentSubjects
		this.type = type
//		this.busy = busy
	}
	
    static constraints = {
		studentName			blank : false
		studentSubjects		blank : true
		busy				nullable: true, blank : true

    }
}
