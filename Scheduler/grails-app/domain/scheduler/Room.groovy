package scheduler

class Room {
	String roomName
	int maxAllowed
	boolean projector
	boolean whiteboard
	boolean tvScreens
	boolean speakerSystem
	
	Room(String roomName, int maxAllowed, boolean projector, boolean whiteboard, boolean tvScreens, boolean speakerSystem){
		this.roomName = roomName
		this.maxAllowed = maxAllowed
		this.projector = projector
		this.whiteboard = whiteboard
		this.tvScreens = tvScreens
		this.speakerSystem = speakerSystem
	}

    static constraints = {
    	roomName			blank : false
		maxAllowed			min : 0
		projector			blank : false
		whiteboard			blank : false
		tvScreens			blank : false
		speakerSystem		blank : false
    }
}
