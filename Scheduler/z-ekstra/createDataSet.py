from random import randint

e = open('Subjects.json', 'w')
f = open('Persons.json', 'w')
g = open('names.txt', 'r')
q = open('subjects.txt', 'r')
r = open('teachers.txt', 'r')

counter = 0
names = []
subjects = []
teachers = []
subjectEnrolls = ["" for x in range(38)]

for line in r:
	teachers.insert(counter, line)
	counter += 1

for line in g:
	names.insert(counter, line)
	counter += 1

counter = 0;
for line in q:
	subjects.insert(counter,line)
	counter += 1

numberOfNames = 999
f.write('{\"persons\" : [\n{\n');

for i in range(0,999):
	counter = randint(0,numberOfNames)
	numberOfNames -= 1
	name = names[counter] 
	names.remove(name)
	chosenSubjects = []
	nameString = ""
	whileCounter = randint(1,8)
	whileCounter2 = 0
	isSubjectTaken = False;
	while whileCounter2 < whileCounter: 
		isSubjectTaken = False
		subjectNo = randint(0,37)
		tempSubject = subjects[subjectNo]
		for q in range(0,whileCounter2):
			if chosenSubjects[q] == tempSubject:
				isSubjectTaken = True
		if isSubjectTaken == False:
			chosenSubjects.insert(whileCounter2, tempSubject)
			subjectEnrolls[subjectNo] += name.rstrip()
			subjectEnrolls[subjectNo] += ","
			whileCounter2 += 1
	for i in range(0,len(chosenSubjects)-1):
		nameString += chosenSubjects[i].rstrip()+','
	nameString += chosenSubjects[len(chosenSubjects)-1].rstrip() + "\"," + "\n"
	f.write("\"name\":\"" + name.rstrip() + '\",' + "\n")
	f.write("\"enrollments\":\"" + nameString)
	f.write("\"type\":\"student\",\n")
	f.write("\"busy\":\"\",\n")
	f.write("},\n{\n")
e.write('{\"imada\":[\n{\n');

teacherNo = 0
for k in range(0,38):
	e.write("\"subjectName\":\"" + subjects[k].rstrip() + "\",\n")
	teacherNo = randint(0,9)
	chosenTeacher = teachers[teacherNo]
	e.write("\"subjectTeacher\":\"" + chosenTeacher.rstrip() + "\",\n")
	tempStudentsEnrolled = subjectEnrolls[k][:-1]
	e.write("\"studentsEnrolled\":\"" + tempStudentsEnrolled + "\",\n")
	e.write("\"subjectWeekStart\":\"5\",\n")
	e.write("\"subjectWeekEnd\":\"26\",\n")
	e.write("\"subjectYear\":\"2016\",\n")
	e.write("},\n{\n")

for k in range(0,9):
	f.write("\"name\":\"" + teachers[k].rstrip() + '\",' + "\n")
	f.write("\"enrollments\":\"\",\n")
	f.write("\"type\":\"teacher\",\n")
	f.write("\"busy\":\"\",\n")
	f.write("},\n{\n")

e.write("}\n]}")
f.write("}\n]}")